from email.policy import default
from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    GENDERS=[
        ('M', "مرد"),
        ('F', "زن")
    ]
    bio = models.TextField(max_length=200, null=True, blank=True)
    gender=models.CharField(choices=GENDERS, max_length=10, default='M')
    image = models.ImageField(upload_to='images/', null=True, blank=True, default=None)

class Vahed(models.Model):
    WEEK_DAYS = [
        ('0', "شنبه"),
        ('1', "یکشنبه"),
        ('2', "دوشنبه"),
        ('3', "سه شنبه"),
        ('4', "چهارشنبه"),
    ]
    department = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    course_number = models.IntegerField()
    group_number = models.IntegerField()
    teacher = models.CharField(max_length=100)
    start_time = models.TimeField()
    end_time = models.TimeField()
    first_day = models.CharField(choices=WEEK_DAYS, max_length=100)
    second_day = models.CharField(choices=WEEK_DAYS, blank=True, null=True, max_length=100)

    def __str__(self):
        return self.name

    def get_string_fields(self):
        excluded_fields = ['id', 'pk']
        field_names = [field.name for field in Vahed._meta.get_fields()
                       if field.name not in excluded_fields]
        values = []
        for field_name in field_names:
            values.append('%s: %s' % (field_name, getattr(self, field_name)))
            if getattr(self, field_name) == None:
                values.pop()
            if Vahed._meta.get_field(field_name).get_internal_type() == "TimeField":
                values[-1] = values[-1][:-3]
        return ' | '.join(values)
