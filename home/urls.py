from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.RegisterView.as_view(), name="register"),
    path('login/', views.login_page, name='login'),
    path('contact_us/', views.contact_us, name='contact_us'),
    path('logout/', views.logout_page, name='logout'),
    path('profile/', views.profile, name="profile"),
    path('edit_proflie/', views.edit_profile, name="edit_profile"),
    path('panel/', views.panel, name="panel"),
    path('vahed.html/', views.VahedCreateView.as_view(), name="vahed"),
    path('list_vaheds/', views.VahedListView.as_view(), name="list_vaheds"),
]

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)