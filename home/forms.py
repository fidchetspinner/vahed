import imp
from django import forms
from .models import Vahed, User



class RegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email']
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput, max_length=100)
    def clean(self):
        cleaned_data=super(RegisterForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            self.add_error('confirm_password', "Password does not match")
        return cleaned_data
    def save(self, commit = True): 
        user = super(RegisterForm, self).save(commit = False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class VahedForm(forms.ModelForm):
    class Meta:
        model=Vahed
        fields="__all__"

class EditUserForm(forms.ModelForm):
    class Meta:
        model=User
        fields=['first_name', 'last_name', 'gender', 'bio', 'image']