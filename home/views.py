from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import CreateView, ListView
from .forms import EditUserForm, RegisterForm, VahedForm
from .models import *
from django.urls import reverse, reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required
import markdown


def index(request):
    return render(request, 'home.html')


class RegisterView(CreateView):
    form_class = RegisterForm
    model = User
    template_name = 'register.html'
    success_url = reverse_lazy('login')


def login_page(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    if request.method == 'POST':
        user = authenticate(request, username=request.POST.get(
            'username', ''), password=request.POST.get('password', ''))
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            return render(request, 'wrong.html')


def contact_us(request):
    if request.method == 'GET':
        return render(request, 'contact_us.html')
    if request.method == 'POST':
        send_mail(
            request.POST['title'],
            request.POST['text']+'\n'+request.POST['email'],
            'amir.zare6@ut.ac.ir',
            ['fidchetspinner@gmail.com'],
            fail_silently=False,
        )
        return HttpResponse("done")


def logout_page(request):
    logout(request)
    return render(request, 'home.html')


@login_required
def profile(request):
    context = {"user": request.user}
    return render(request, 'profile.html', context)


@login_required
def edit_profile(request):
    if request.method == 'GET':
        context = {'form': EditUserForm(instance=request.user)}
        return render(request, 'edit_profile.html', context)
    if request.method == 'POST':
        if (request.POST.get("first_name", "")) is not "":
            request.user.first_name = request.POST.get("first_name", "")
        if (request.POST.get("last_name", "")) is not "":
            request.user.last_name = request.POST.get("last_name", "")
        if (request.POST.get("bio", "")) is not "":
            request.user.bio = markdown.markdown(request.POST.get("bio", ""))
        if (request.FILES.get("image", None)) is not None:
            request.user.image = request.FILES['image']
        request.user.gender = request.POST.get("gender", "")
        request.user.save()
        return render(request, 'profile.html')


def panel(request):
    return render(request, 'panel.html')


class VahedListView(LoginRequiredMixin, ListView):
    model = Vahed
    template_name = 'list_vaheds.html'
    context_object_name = 'vaheds'


class VahedCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    form_class = VahedForm
    model = Vahed
    template_name = 'vahed.html'
    success_url = reverse_lazy('vahed')

    def test_func(self):
        return self.request.user.is_superuser
